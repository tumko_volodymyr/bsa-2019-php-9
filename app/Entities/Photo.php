<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Photo
 * @package App\Entities
 *
 * @property string $original_photo
 * @property string $photo_100_100
 * @property string $photo_150_150
 * @property string $photo_250_250
 * @property string $status
 * @property User $user
 */
class Photo extends Model
{

    const STATUS_UPLOADED = 'UPLOADED';
    const STATUS_PROCESSING= 'PROCESSING';
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAIL = 'FAIL';

    protected $fillable = [
        'user_id', 'original_photo', 'status'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function setPhotoBySize (int $size, string $path)
    {
        $propertyName = sprintf('photo_%s_%s', $size, $size);
        $this->setAttribute($propertyName, $path);
    }
}
