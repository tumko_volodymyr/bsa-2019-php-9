<?php

namespace App\Http\Controllers;

use App\Services\UserPhotoUploader;
use App\Values\Image;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PhotoController extends Controller
{
    public function upload (Request $request, UserPhotoUploader $photoUploader)
    {
        try {
            $request->validate([
                'photo' => 'required|image',
            ]);
            $photo = $request->file('photo');

            $photoUploader->upload(new Image($photo));

            return response()->json([
                'status' => 'success'
            ], 200);
        } catch (ValidationException $e) {
            return response()->json([ "error" => $e->errors() ], 400);
        }
    }
}
