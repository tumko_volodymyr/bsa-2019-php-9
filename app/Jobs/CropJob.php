<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Services\Contracts\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $photo;

    public function __construct(Photo $photo)
    {
        $this->photo = $photo;
    }

    public function handle(PhotoService $photoService)
    {

        $this->photo->status = Photo::STATUS_PROCESSING;
        $originalPhoto = $this->photo->original_photo;
        $tmp = explode('/', $originalPhoto);
        $name = array_pop($tmp);
        $rootDir = Config::get('filesystems.photo_images_root_dir');
        $userId = $this->photo->user->id;
        $path = $rootDir.'/'.$userId;
        [$name, $ext ] = explode('.', $name);

        foreach ([100, 150, 250] as $item){
            $cropped = $photoService->crop($originalPhoto, $item, $item);
            $newPath = $path.sprintf('/%s%sx%s.%s', $name, $item, $item, $ext);
            Storage::delete($newPath);
            Storage::move($cropped, $newPath);
            $this->photo->setPhotoBySize($item, $newPath);
        }

        $this->photo->status = Photo::STATUS_SUCCESS;
        $this->photo->save();

        $this->photo->user->notify(new ImageProcessedNotification($this->photo));

    }

    public function failed(\Exception $exception)
    {
        $this->photo->status = Photo::STATUS_FAIL;
        $this->photo->save();
        $this->photo->user->notify(new ImageProcessingFailedNotification($this->photo));
    }


}
