<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Storage;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    public $photo;

    public function __construct(Photo $photo)
    {

        $this->photo = $photo;
    }

    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Dear '.$notifiable->name.',')
                    ->line('Photos have been successfully uploaded and processed.')
                    ->line('Here are links to the images:')
                    ->line(Storage::url($this->photo->photo_100_100))
                    ->line(Storage::url($this->photo->photo_150_150))
                    ->line(Storage::url($this->photo->photo_250_250))
                    ->line('Thanks!');
    }

    public function toArray($notifiable)
    {
        return [
            'status' => $this->photo->status,
            'photo_100_100' => $this->photo->photo_100_100,
            'photo_150_150' => $this->photo->photo_150_150,
            'photo_250_250' => $this->photo->photo_250_250,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'status' => mb_strtolower($this->photo->status),
                'photo_100_100' => Storage::url($this->photo->photo_100_100),
                'photo_150_150' => Storage::url($this->photo->photo_150_150),
                'photo_250_250' => Storage::url($this->photo->photo_250_250),
            ]
        );
    }
}
