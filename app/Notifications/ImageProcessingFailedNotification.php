<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    private $photo;

    public function __construct(Photo $photo)
    {
        //
        $this->photo = $photo;
    }

    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            [
                'status' => mb_strtolower($this->photo->status),
                'photo_100_100' => $this->photo->photo_100_100,
                'photo_150_150' => $this->photo->photo_150_150,
                'photo_250_250' => $this->photo->photo_250_250,
            ]
        );
    }
}
