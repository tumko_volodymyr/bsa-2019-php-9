<?php


namespace App\Services\Contracts;


use App\Values\Image;

interface UserPhotoUploader
{
    public function upload (Image $file);
}
