<?php


namespace App\Services;


use App\Entities\Photo;
use App\Jobs\CropJob;
use App\Services\Contracts\UserPhotoUploader as IUserPhotoUploader;
use App\Values\Image;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;

class UserPhotoUploader implements IUserPhotoUploader
{
    public function upload (Image $file)
    {
        $rootDir = Config::get('filesystems.photo_images_root_dir');
        $userId = Auth::id();
        $path = $rootDir.'/'.$userId;
        $filePath = Storage::putFileAs(
            $path,
            $file->getImage(),
            $file->getImage()->getClientOriginalName(),
            'public'
        );

        $photo = Photo::create([
            'original_photo' => $filePath,
            'user_id' => $userId,
            'status' => Photo::STATUS_UPLOADED
        ]);

        CropJob::dispatch($photo);
    }
}
