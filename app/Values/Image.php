<?php


namespace App\Values;


use Illuminate\Http\UploadedFile;

class Image
{
    private $image;

    public function __construct(UploadedFile $image)
    {
        $this->image = $image;
    }

    public function getImage(): UploadedFile
    {
        return $this->image;
    }
}
